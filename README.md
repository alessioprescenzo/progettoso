# Z64 Emulator

This emulator is aimed to run code written specifically for the z64 processor, a non-existing didactic processor, studied in the "Calculators architecture" course 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
The project is not even closed to the conclusion but it's not one of my main one. 
In the current state it correctly fetch the information from an array of 32 bit components (istruction);


### **Prerequisites**

You only need a C compiler in order to run this:

#### Ubuntu
    $ sudo apt-get install build-essential

#### MacOS
    $ xcode-select --install

### **Building**

To build this you just need to type in the terminal:
    
    make



## Running the tests

WIP

## Deployment

WIP

## Built With

WIP

## Contributing

WIP


## Authors

* **Alessio Prescenzo** - *Developer* - [alessioprescenzo](https://gitlab.com/alessioprescenzo)
* **Marco Mormando** - *Developer* - [tremme24](https://gitlab.com/tremme24)

## License

WIP

## Acknowledgments

WIP