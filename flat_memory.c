#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "flat_memory.h"
 
void initMemory(flat_memory_v2* mem, __uint8_t* stack, __uint32_t size){
    if(size>MAXSIZE || size==0){ 
        printf("Memory Size Buffer error, MAXSIZE = %d Byte\n",MAXSIZE); 
        exit(-1);
    }
    mem->size=size;
    mem->cells=stack;
    printf("Successfully allocated flat_memory, Cells: %d!!\n",size);
    sleep(1);
}