#pragma once
#include "istruction.h"
#define MAXSIZE 102400

typedef struct flat_memory_v2{
    __uint32_t size;
    __uint8_t* cells;
}flat_memory_v2;

void initMemory(flat_memory_v2* mem, __uint8_t* stack,__uint32_t size);
