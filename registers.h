#pragma once
#include "istruction.h"
#include "stdint.h"

typedef struct rflags{
    __uint8_t OF: 1; 
    __uint8_t DF: 1;
    __uint8_t IF: 1;
    __uint8_t SZ: 1;
    __uint8_t ZF: 1;
    __uint8_t PF: 1;
    __uint8_t CF: 1;
}rflags;

typedef struct registers{
    __uint64_t RAX;
    __uint64_t RBX;
    __uint64_t RCX;
    __uint64_t RDX;
    __uint64_t RSP;
    __uint64_t RBP;
    __uint64_t RSI;
    __uint64_t RDI;
    __uint64_t R8;
    __uint64_t R9;
    __uint64_t R10;
    __uint64_t R11;
    __uint64_t R12;
    __uint64_t R13;
    __uint64_t R14;
    __uint64_t MDR;
    __uint64_t MAR;
    __uint64_t RIP;
    rflags RFLAGS;
    istruction IR;
}registers;

void initRegisters(registers* reg);