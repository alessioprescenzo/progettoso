CC=gcc
CCOPTS=--std=gnu99
AR=ar


HEADERS=istruction.h\
		flat_memory.h\
		registers.h\

OBJS=registers.o\
		flat_memory.o\
		istruction.o
	
LIBS=libz64.a

BINS=z64

.phony: clean all

all: $(LIBS) $(BINS)

%.o:	%.c $(HEADERS)
	$(CC) $(CCOPTS) -c -o $@  $<

libz64.a: $(OBJS) $(HEADERS)
	$(AR) -rcs $@ $^
	$(RM) $(OBJS)

z64:	z64.c $(LIBS)
	$(CC) $(CCOPTS) -o $@ $^
clean:
	rm -rf *.o *~ $(LIBS) $(BINS)
