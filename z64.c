#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "registers.h"
#include "flat_memory.h"
#include "alu.h"
#define RIPSTEP 8

registers reg;//Program Counter -> Needs to point to init of the emulator
flat_memory_v2 memory;
__uint8_t stack[MAXSIZE] = { 0 };
__uint8_t running=0;

int main(int argc,char** argv){
    
    //preparing environment
    initRegisters(&reg);
    initMemory(&memory,stack, MAXSIZE);
    printf("Size struct rflags: %lu\n", sizeof(rflags));
    printf("Size struct istruction %lu\n", sizeof(istruction));        
    printf("Size stack: %lu\n", sizeof(stack));                
    sleep(1)
    
    for(;;){
        //Fetch;
        reg.MAR=reg.RIP;
        memcpy(&reg.MDR,memory.cells+reg.MAR,8);
        reg.RIP+=RIPSTEP;
        memcpy(&reg.IR,&reg.MDR,8);
        printf("Fetch done, MAR: 0x%llx, RIP: 0x%llx\n",reg.MAR,reg.RIP);
        printf("\tCampo Class: %x Campo Type:%x \n", reg.IR.class, reg.IR.type);
        printf("\tCampo SS: %x Campo DS: %x Campo DI: %x Campo MEM: %x\n", reg.IR.ss,reg.IR.ds,reg.IR.di,reg.IR.mem);
        printf("\tCampo BP: %x Campo IP: %x Campo SCALA: %x Campo INDEXREG: %x\n", reg.IR.bp, reg.IR.ip, reg.IR.scale,reg.IR.index);
        printf("\tShortimm: %x\n", reg.IR.shortImm);            
        //decode((instruction*) reg.RIP);
        
        //execute();
        sleep(1);
        //Exit if Overflow
        if(reg.RIP==(memory.size)*8){
            printf("Reached RIP==MEMSize, breaking execution!!\n");
            break;
        }
    }

    printf("Exiting...\n");
        //closing all necessaries
        //
    printf("Done, bye!\n");
    return 0;
}