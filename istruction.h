#pragma once
#include <stdint.h>

typedef struct instruction{
    __uint8_t type:4;      //type opcode
    __uint8_t class:4;    //class opcode
    
    __uint8_t mem:2;      //memory operand (mode)
    __uint8_t di:2;      //displacement/immediate (mode) 
    __uint8_t ds:2;      //size dest (mode)
    __uint8_t ss:2;      //size source (mode)
   
    __uint8_t index:4;       //reg index (SIB)
    __uint8_t scale:2;       //scala (SIB)
    __uint8_t ip:1;       //if  index (SIB)
    __uint8_t bp:1;       //if base (SIB)
    
    __uint8_t dest:4;        //reg dest/base (R/M)
    __uint8_t source:4;        //reg source/base (R/M)
    
    __uint32_t shortImm;   //32bit integer; On sequential cells of memory we have the first byte that's less and on...
} istruction;

typedef struct longImm{
    __uint64_t longImm; 
}longImm;
